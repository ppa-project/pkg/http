module ci.tno.nl/gitlab/ppa-project/pkg/http

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/rs/zerolog v1.19.0
	gotest.tools v2.2.0+incompatible
)
