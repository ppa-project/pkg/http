// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

// Role is an enum to define roles for a Config
type Role int

const (
	// None defines no role
	None Role = iota
	// Self defines the self role
	Self
	// Node defines the node role
	Node
	// GUI defines the GUI role
	GUI
)

// Config holds the base http configuration for the daemon
type Config struct {
	Name        string
	Port        int
	Host        string
	BindHost    string
	Role        Role
	TLSCertFile string
	TLSKeyFile  string
}

// CertSignature returns the signature of the certificate
func (c *Config) CertSignature() []byte {
	certs := ParseCertificates(c.TLSCertFile)
	return certs[0].Signature
}
