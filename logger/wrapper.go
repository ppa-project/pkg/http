// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package logger is responsible for logging
package logger

import (
	"log"

	"github.com/rs/zerolog"
	l "github.com/rs/zerolog/log"
)

// NewZeroLogTraceLogger constructs a ZeroLogTraceLogger
func NewZeroLogTraceLogger() *log.Logger {
	return log.New(&FwdToZeroLogTraceWriter{logger: &l.Logger}, "", 0)
}

// FwdToZeroLogTraceWriter is a wrapper for a zerolog.Logger
type FwdToZeroLogTraceWriter struct {
	logger *zerolog.Logger
}

func (fw *FwdToZeroLogTraceWriter) Write(p []byte) (n int, err error) {
	fw.logger.Trace().Msg(string(p))
	return len(p), nil
}
