// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

// var c h.Config
// var pool map[string]h.Config
// var hc *HTTPController

// func TestMain(m *testing.M) {
// 	test.Setup()

// 	code := m.Run()
// 	os.Exit(code)
// }

// func Setup() {
// 	c, pool = getConfig("PartyA")

// 	router := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		fmt.Fprintf(w, "Hello!")
// 	})

// 	hc = NewHTTPController(c, pool)
// 	hc.RunHTTPServerWithRouter(router)
// }

// func Teardown() {
// 	c := make(chan struct{})
// 	_, cancel := hc.Shutdown(1*time.Second, c)
// 	defer cancel()
// 	<-c

// 	os.RemoveAll("tls")
// }

// func TestCertNotInPool(t *testing.T) {
// 	Setup()
// 	defer Teardown()
// 	// Try to send message PartyB -> PartyA
// 	c, pool = getConfig("PartyB")

// 	ms, _ := client.NewMessageSender(c, pool)

// 	m := &client.Message{
// 		Sender:   "PartyB",
// 		Receiver: "PartyA",
// 		QueryID:  "1a2b3c",
// 		StepID:   "1",
// 		Body:     []byte(""),
// 	}
// 	err := ms.Send(m)
// 	assert.Error(t, err)
// 	Teardown()
// }

// func TestCertAddToPool(t *testing.T) {
// 	Setup()
// 	defer Teardown()
// 	c, pool = getConfig("PartyB")

// 	// Add PartyB configuration to the PartyA HTTPController
// 	partyBCrt, _ := ioutil.ReadFile(c.TLSCertFile)
// 	go func() {
// 		hc.NodeConfigurationUpdate("PartyB", "localhost", string(partyBCrt), "something else")
// 	}()
// 	time.Sleep(5 * time.Second)

// 	// Try to send message PartyB -> PartyA
// 	ms, _ := client.NewMessageSender(c, pool)

// 	m := &client.Message{
// 		Sender:   "PartyB",
// 		Receiver: "PartyA",
// 		QueryID:  "1a2b3c",
// 		StepID:   "1",
// 		Body:     []byte(""),
// 	}
// 	err := ms.Send(m)
// 	assert.NoError(t, err)
// }

// func TestAddToPoolMultiple(t *testing.T) {
// 	CertAddToPoolMultiple(t, false)
// 	CertAddToPoolMultiple(t, true)
// }

// func CertAddToPoolMultiple(t *testing.T, sleep bool) {
// 	Setup()
// 	defer Teardown()
// 	c, pool = getConfig("PartyB")

// 	// Add PartyB configuration to the PartyA HTTPController
// 	partyBCrt, _ := ioutil.ReadFile(c.TLSCertFile)
// 	for i := 0; i < 10; i++ {
// 		go func() {
// 			hc.NodeConfigurationUpdate("PartyB", "localhost", string(partyBCrt), "something else")
// 		}()
// 		if sleep {
// 			time.Sleep(100 * time.Millisecond)
// 		}
// 	}
// 	time.Sleep(5 * time.Second)

// 	// Try to send message PartyB -> PartyA

// 	ms, _ := client.NewMessageSender(c, pool)

// 	m := &client.Message{
// 		Sender:   "PartyB",
// 		Receiver: "PartyA",
// 		QueryID:  "1a2b3c",
// 		StepID:   "1",
// 		Body:     []byte(""),
// 	}
// 	err := ms.Send(m)
// 	assert.NoError(t, err)
// }

// func getConfig(party string) (h.Config, map[string]h.Config) {
// 	testDir, _ := os.Getwd()
// 	os.Chdir(fmt.Sprintf("../testdata/%s", party))
// 	defer os.Chdir(testDir)

// 	viper.Reset()
// 	viper.SetConfigFile("mpc_node-localhost.toml")
// 	viper.ReadInConfig()

// 	httpConfig, pool := GetHTTPConfig(party)

// 	httpConfig.TLSCertFile, _ = filepath.Abs(httpConfig.TLSCertFile)
// 	httpConfig.TLSKeyFile, _ = filepath.Abs(httpConfig.TLSKeyFile)

// 	for _, node := range pool {
// 		node.TLSCertFile, _ = filepath.Abs(node.TLSCertFile)
// 		node.TLSKeyFile, _ = filepath.Abs(node.TLSKeyFile)
// 		pool[node.Name] = node
// 	}

// 	return httpConfig, pool
// }

// // GetConfig parses node configurations from the config file
// func GetHTTPConfig(thisNodeName string) (h.Config, map[string]h.Config) {
// 	var nodes []h.Config
// 	err := viper.UnmarshalKey("nodes", &nodes)
// 	if err != nil {
// 		log.Fatal().Err(err).Msg("Error getting config")
// 	}

// 	// Assign own configuration to c, and the others to the pool
// 	var httpConfig h.Config
// 	pool := make(map[string]h.Config)
// 	for _, node := range nodes {
// 		if node.Name == thisNodeName {
// 			httpConfig = node
// 		} else {
// 			pool[node.Name] = node
// 		}
// 	}

// 	if httpConfig.Name != thisNodeName {
// 		log.Fatal().Msgf("Node to start is unknown: %s", thisNodeName)
// 	}

// 	return httpConfig, pool
// }
