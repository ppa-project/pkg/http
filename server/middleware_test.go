// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/test"
	"github.com/gin-gonic/gin"
	"gotest.tools/assert"
)

func TestMain(m *testing.M) {
	test.Setup()
	gin.SetMode(gin.TestMode)
	code := m.Run()
	os.Exit(code)
}

func TestMiddlewareRoleNotSet(t *testing.T) {
	router := gin.New()
	router.Use(HasRole(h.Self))
	router.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 403, w.Code)
}

func TestMiddlewareRoleSet(t *testing.T) {
	router := gin.New()
	router.Use(func(c *gin.Context) {
		c.Set("role", h.Self)
		c.Next()
	})
	router.Use(HasRole(h.Self))
	router.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "pong", w.Body.String())
}

func TestMiddlewareRoleSetScoped(t *testing.T) {
	router := gin.New()
	router.Use(func(c *gin.Context) {
		c.Set("role", h.Self)
		c.Next()
	})
	router.Use(HasRole(h.Self))
	{
		router.GET("/ping", func(c *gin.Context) {
			c.String(200, "pong")
		})
	}

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "pong", w.Body.String())
}

func TestMiddlewareRoleEmptyConfig(t *testing.T) {
	config := h.Config{}
	router := gin.New()
	router.Use(func(c *gin.Context) {
		c.Set("role", config.Role)
		c.Next()
	})
	router.Use(HasRole(h.Self))
	router.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 403, w.Code)
}
