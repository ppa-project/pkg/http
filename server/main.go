// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package server provides HTTP server
package server

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
	"sync"
	"time"

	"github.com/rs/zerolog/log"

	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/logger"
)

const (
	// ServerRestartTimeout time to wait for new configuration changes before restarting the HTTP server
	ServerRestartTimeout = 3 * time.Second
)

// HTTPController To instantiate and run a HTTP Server
type HTTPController struct {
	sync.Mutex
	clientAuth bool
	config     h.Config
	pool       map[string]h.Config
	server     *http.Server
	router     *http.Handler
}

// NewHTTPController constructs a new HTTP Controller
func NewHTTPController(bindhost string, port int) *HTTPController {
	config := h.Config{
		BindHost: bindhost,
		Port:     port,
	}
	return &HTTPController{clientAuth: false, config: config}
}

// NewHTTPControllerWithClientAuth constructs a new HTTP Controller with client certificate authentication
func NewHTTPControllerWithClientAuth(config h.Config, pool map[string]h.Config) *HTTPController {
	return &HTTPController{clientAuth: true, config: config, pool: pool}
}

// CreateHTTPServerWithRouter creates an HTTP Server with a specified router
func (hc *HTTPController) CreateHTTPServerWithRouter(router http.Handler) (*http.Server, error) {
	hc.router = &router
	return hc.createHTTPServer()
}

// RunHTTPServerWithRouter runs an HTTP Server with a specified router
func (hc *HTTPController) RunHTTPServerWithRouter(router http.Handler) {
	hc.router = &router
	hc.runHTTPServer()
}

// runHTTPServer runs an HTTP Server
func (hc *HTTPController) runHTTPServer() {
	if hc.router == nil {
		log.Fatal().Msg("Failed to run HTTP Server: no router defined")
	}

	var err error
	hc.server, err = hc.createHTTPServer()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to run HTTP Server")
	}

	if hc.config.TLSCertFile != "" && hc.config.TLSKeyFile != "" { // TLS
		logMsg := ""
		if hc.server.TLSConfig != nil && hc.server.TLSConfig.ClientAuth >= tls.VerifyClientCertIfGiven {
			logMsg = fmt.Sprintf("Starting HTTP server with TLS and client authentication on %s", hc.server.Addr)
		} else {
			logMsg = fmt.Sprintf("Starting HTTP server with TLS on %s", hc.server.Addr)
		}

		if hc.config.Host != "" && hc.config.Host != hc.config.BindHost {
			log.Info().Msgf("%s (external host %s)", logMsg, hc.config.Host)
		} else {
			log.Info().Msg(logMsg)
		}
		go func() {
			err := hc.server.ListenAndServeTLS(hc.config.TLSCertFile, hc.config.TLSKeyFile)
			if err != nil && err != http.ErrServerClosed {
				log.Fatal().Err(err).Msg("Failed to run HTTPS Server")
			}
		}()
	} else { // No TLS
		if hc.config.Host != "" && hc.config.Host != hc.config.BindHost {
			log.Info().Msgf("Starting HTTP server on %s (external host %s)", hc.server.Addr, hc.config.Host)
		} else {
			log.Info().Msgf("Starting HTTP server on %s", hc.server.Addr)
		}
		go func() {
			err := hc.server.ListenAndServe()
			if err != nil && err != http.ErrServerClosed {
				log.Fatal().Err(err).Msg("Failed to run HTTP Server")
			}
		}()
	}
}

// createHTTPServer creates an HTTP Server
func (hc *HTTPController) createHTTPServer() (*http.Server, error) {
	if hc.router == nil {
		return nil, errors.New("cannot create HTTP Server: no router defined")
	}

	var tlsConfig *tls.Config

	if hc.clientAuth {
		// Add certificates of other nodes, to be able to do client-auth
		caCertPool := x509.NewCertPool()
		for _, otherNode := range hc.pool {
			caCert, err := ioutil.ReadFile(otherNode.TLSCertFile)
			if err != nil {
				return nil, err
			}
			if ok := caCertPool.AppendCertsFromPEM(caCert); !ok {
				return nil, errors.New("failed to add certificate to certpool")
			}
		}

		// Setup HTTPS client
		tlsConfig = &tls.Config{
			MinVersion: tls.VersionTLS12,
			ClientCAs:  caCertPool,
			ClientAuth: tls.RequireAndVerifyClientCert,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			},
		}
	}

	return &http.Server{
		Addr:      hc.config.BindHost + ":" + strconv.Itoa(hc.config.Port),
		TLSConfig: tlsConfig,
		Handler:   *hc.router,
		ErrorLog:  logger.NewZeroLogTraceLogger(),
	}, nil
}

// Shutdown shuts down the HTTP gracefully within the given timeout
func (hc *HTTPController) Shutdown(timeout time.Duration, closeChan chan struct{}) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)

	go func() {
		if err := hc.server.Shutdown(ctx); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP Server Shutdown: %v", err)
		}
		close(closeChan)
	}()

	return ctx, cancel
}

var l = &sync.Mutex{}
var timeoutTimer *time.Timer

// ScheduleRestartHTTPServer schedules a HTTP server restart
// The purpose of this method is to prevent multiple restarts
// of the HTTP Server within a small timebound.
// When this method is called again within the timeout bounds,
// the timer is reset. When the timer fires, the method will call
// the RestartHTTPServer method.
func (hc *HTTPController) ScheduleRestartHTTPServer() {
	l.Lock()
	if timeoutTimer == nil {
		timeoutTimer = time.NewTimer(ServerRestartTimeout)
	} else {
		timeoutTimer.Reset(ServerRestartTimeout)
		l.Unlock()
		return
	}
	l.Unlock()

	go func() {
		<-timeoutTimer.C
		l.Lock()
		timeoutTimer = nil
		hc.RestartHTTPServer()
		l.Unlock()
	}()
}

// RestartHTTPServer restarts the HTTP server immediately.
func (hc *HTTPController) RestartHTTPServer() {
	hc.Lock()
	defer hc.Unlock()

	log.Info().Msg("Restarting HTTP Server...")
	idleHTTPConnsClosed := make(chan struct{})
	_, cancel := hc.Shutdown(10*time.Second, idleHTTPConnsClosed)
	defer cancel()
	<-idleHTTPConnsClosed

	hc.runHTTPServer() // Non-blocking
}

// NodeConfigurationUpdate implements the coordinator.NodeConfigListener interface
func (hc *HTTPController) NodeConfigurationUpdate(org, ip, port, cert, _ string) {
	if org == hc.config.Name {
		return
	}
	if ip == "" || port == "" || cert == "" {
		log.Error().Str("org", org).Msg("Empty configuration received from organization")
		return
	}
	log.Debug().
		Str("org", org).
		Str("ip", ip).
		Msg("Received configuration update")
	err := os.MkdirAll(path.Join("tls", "ext"), 0744)
	if err != nil {
		log.Error().Err(err).Msg("Cannot create directory")
	}

	portInt, err := strconv.Atoi(port)
	if err != nil {
		log.Error().Err(err).Msg("Cannot convert port to integer")
	}

	config := h.Config{
		Name:        org,
		Port:        portInt,
		Host:        ip,
		Role:        h.Node,
		TLSCertFile: path.Join("tls", "ext", org+".crt"),
		TLSKeyFile:  "",
	}

	hc.Lock()
	defer hc.Unlock()
	hc.pool[org] = config
	err = h.ImportCertificate(org, hc.pool, []byte(cert))
	if err == nil {
		log.Info().
			Str("org", org).
			Str("ip", ip).
			Str("certSig", hex.EncodeToString(config.CertSignature())).
			Msg("Applying configuration update")
		go func() {
			hc.ScheduleRestartHTTPServer()
		}()
	}
}

// GuiConfigurationUpdate implements the coordinator.GuiConfigListener interface
func (hc *HTTPController) GuiConfigurationUpdate(ip, cert string) {
	if ip == "" || cert == "" {
		log.Error().Msg("Empty GUI configuration received")
		return
	}
	log.Debug().
		Str("ip", ip).
		Msg("Received GUI configuration update")
	err := os.MkdirAll(path.Join("tls", "ext"), 0744)
	if err != nil {
		log.Error().Err(err).Msg("Cannot create directory")
	}
	config := h.Config{
		Name:        "GUI",
		Port:        0,
		Host:        ip,
		Role:        h.GUI,
		TLSCertFile: path.Join("tls", "ext", "GUI.crt"),
		TLSKeyFile:  "",
	}

	hc.Lock()
	defer hc.Unlock()
	hc.pool["GUI"] = config
	err = h.ImportCertificate("GUI", hc.pool, []byte(cert))
	if err == nil {
		log.Info().
			Str("ip", ip).
			Str("certSig", hex.EncodeToString(config.CertSignature())).
			Msg("Applying GUI configuration update")
		go func() {
			hc.ScheduleRestartHTTPServer()
		}()
	}
}
