// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"bytes"
	"net/http"
	"regexp"
	"time"

	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// AuthCheck Makes sure that at least one verified client certificate is used for this connection.
func AuthCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.TLS == nil {
			Error(c, http.StatusForbidden, "TLS Connection required")
			return
		}
		if len(c.Request.TLS.PeerCertificates) < 1 {
			Error(c, http.StatusForbidden, "Valid client certificate required to participate.")
		}
	}
}

// InjectClientIdentity Matches the signature of the client certificate with the cerificate signature from the pool.
// Based on the signature, the identity of the sender is derived.
func (hc *HTTPController) InjectClientIdentity() gin.HandlerFunc {
	return func(c *gin.Context) {
		matched := false
		for _, cert := range c.Request.TLS.VerifiedChains[0] {
			for _, node := range hc.pool {
				if bytes.Equal(cert.Signature, node.CertSignature()) {
					c.Set("sender", node.Name)
					c.Set("receiver", hc.config.Name)
					c.Set("role", node.Role)

					c.Next()
					matched = true
				}
			}
		}
		if !matched {
			Error(c, http.StatusForbidden, "Client certificate signature mismatch")
		}
	}
}

// HasRole Checks whether the connected client has the required role
func HasRole(role h.Role) gin.HandlerFunc {
	return func(c *gin.Context) {
		if r, ok := c.Get("role"); ok && r == role {
			c.Next()
			return
		}
		Error(c, http.StatusForbidden, "Access Forbidden")
	}
}

// RouteLogIncoming Logs the routes of the incoming requests
func RouteLogIncoming() gin.HandlerFunc {
	return func(c *gin.Context) {
		sender, exists := c.Get("sender")
		if !exists {
			sender = c.Request.RemoteAddr
		}

		log.Trace().
			Str("sender", sender.(string)).
			Str("method", c.Request.Method).
			Str("url", c.Request.URL.Path).
			Msgf("  [%s] --> %s %s", sender, c.Request.Method, c.Request.URL.Path)

		c.Next()
	}
}

// RouteMetrics Logs the metrics of the incoming requests
func RouteMetrics() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		c.Next()
		elapsed := time.Since(start)

		sender, exists := c.Get("sender")
		if !exists {
			sender = c.Request.RemoteAddr
		}

		switch code := c.Writer.Status(); {
		case (code >= 400) && (code <= 599):
			log.Debug().
				Str("sender", sender.(string)).
				Str("method", c.Request.Method).
				Str("url", c.Request.URL.Path).
				Int("status", c.Writer.Status()).
				Dur("duration", elapsed).
				Msgf("  [%s] xxx %s %s %d %dms", sender, c.Request.Method, c.Request.URL.Path, code, elapsed.Milliseconds())
		default:
			log.Trace().
				Str("sender", sender.(string)).
				Str("method", c.Request.Method).
				Str("url", c.Request.URL.Path).
				Int("status", c.Writer.Status()).
				Dur("duration", elapsed).
				Msgf("  [%s] <-- %s %s %d %dms", sender, c.Request.Method, c.Request.URL.Path, code, elapsed.Milliseconds())
		}
	}
}

// LogIncomingIPs logs the IP addresses of incoming connections
func LogIncomingIPs() gin.HandlerFunc {
	seen := make(map[string]string)
	return func(c *gin.Context) {
		sender := c.GetString("sender")
		exp := regexp.MustCompile(`^(.*):\d+$`)
		matches := exp.FindStringSubmatch(c.Request.RemoteAddr)
		reqip := matches[1]
		if sender == "" {
			log.Debug().Msg("LogIncomingIPs: sender not found in context")
			c.Next()
			return
		}
		ip, ok := seen[sender]
		if !ok && ip != reqip {
			log.Info().Str("ip", reqip).Str("sender", sender).Msg("New IP address for incoming connection")
			seen[sender] = reqip
		}
		c.Next()
	}
}

// Success returns a success JSON
func Success(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"success": true,
	})
}

// Error returns an error JSON
func Error(c *gin.Context, code int, obj string) {
	c.AbortWithStatusJSON(code, gin.H{
		"error": obj,
	})
}
