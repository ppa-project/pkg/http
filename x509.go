// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package http provides HTTP related utilities, configurations and middleware
package http

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"path"
	"time"

	"github.com/rs/zerolog/log"
)

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
			os.Exit(2)
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}

// ParseCertificates parses a cert file to x509 certificates
func ParseCertificates(certPath string) []*x509.Certificate {
	caCert, e := ioutil.ReadFile(path.Clean(certPath))
	if e != nil {
		log.Fatal().Err(e).Str("path", certPath).Msg("Failed to read certificate")
	}
	block, _ := pem.Decode(caCert)
	if block == nil || block.Bytes == nil {
		log.Fatal().Err(e).Str("path", certPath).Msg("Failed to read certificate PEM")
	}
	certs, e2 := x509.ParseCertificates(block.Bytes)
	if e2 != nil {
		log.Fatal().Err(e2).Str("path", certPath).Msg("Failed to parse certificate")
	}
	return certs
}

// GetCertificatePEMText transforms a certificate to PEM format
// In: x509.Certificate bytes
// Out: PEM Certificate (ASCII Encoded)
func GetCertificatePEMText(derBytes []byte) []byte {
	out := &bytes.Buffer{}
	if err := pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		return []byte{}
	}
	return out.Bytes()
}

// GetPrivkeyPEMText transforms a private key to PEM format
// In: *{rsa, ecdsa}.PrivateKey
// Out: PEM Private key (ASCII Encoded)
func GetPrivkeyPEMText(privkey interface{}) []byte {
	out := &bytes.Buffer{}
	if err := pem.Encode(out, pemBlockForKey(privkey)); err != nil {
		return []byte{}
	}
	return out.Bytes()
}

// ImportCertificate stores the given certificate for a node
func ImportCertificate(nodeName string, pool map[string]Config, certificate []byte) error {
	node, ok := pool[nodeName]
	if !ok {
		log.Error().Msgf("Node name %s not found in pool", nodeName)
		return fmt.Errorf("node name %s not found in pool", nodeName)
	}
	err := ioutil.WriteFile(node.TLSCertFile, certificate, 0600)
	if err != nil {
		log.Error().Err(err).Msg("Failed to write certificate to file")
		return err
	}
	return nil
}

// GenerateX509Certificate generates a new self signed x509 certificate
func GenerateX509Certificate(config Config) {
	GenerateX509CertificateWithAltNames(config, []string{})
}

// GenerateX509CertificateWithAltNames generates a new self signed x509 certificate
func GenerateX509CertificateWithAltNames(config Config, altNames []string) {
	log.Info().Msg("Generate new X509 certificate and keypair")
	privkey, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to generate ECDSA key")
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Country:      []string{"NL"},
			Organization: []string{fmt.Sprintf("%s (PPA)", config.Name)},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(10, 0, 0), // 10 years

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
		IsCA:                  true,
		BasicConstraintsValid: true,
	}

	allNames := append([]string{config.Host}, altNames...)

	for _, name := range allNames {
		ip := net.ParseIP(name)
		if ip != nil {
			// name is an IP address
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			// name is a domainname
			template.DNSNames = append(template.DNSNames, name)
		}
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(privkey), privkey)
	if err != nil {
		log.Fatal().Err(err).Msgf("Failed to create certificate")
	}

	// Write certificate
	if err = ioutil.WriteFile(config.TLSCertFile, GetCertificatePEMText(derBytes), 0600); err != nil {
		log.Fatal().Err(err).Msgf("Failed to write certificate")
	}

	// Write private key
	if err = ioutil.WriteFile(config.TLSKeyFile, GetPrivkeyPEMText(privkey), 0600); err != nil {
		log.Fatal().Err(err).Msgf("Failed to write key")
	}
}

// SetupTLSCertificate checks whether a valid x509 keypair is present for this node.
// If not, it will generate a new certificate.
func SetupTLSCertificate(config Config) {
	SetupTLSCertificateWithAltNames(config, []string{})
}

// SetupTLSCertificateWithAltNames checks whether a valid x509 keypair is present for this node.
// If not, it will generate a new certificate.
func SetupTLSCertificateWithAltNames(config Config, altNames []string) {
	certs, err := tls.LoadX509KeyPair(config.TLSCertFile, config.TLSKeyFile)
	if err != nil {
		// Failed to load X509 Keypair
		// Check whether both cert and key are missing
		_, errCert := ioutil.ReadFile(config.TLSCertFile)
		_, errKey := ioutil.ReadFile(config.TLSKeyFile)
		if errCert != nil && errKey != nil {
			// Both cert and key are missing
			// Touch cert and key
			err = os.MkdirAll(path.Dir(config.TLSCertFile), 0744)
			if err != nil {
				log.Fatal().
					Err(err).
					Str("path", path.Dir(config.TLSCertFile)).
					Msg("I/O error: Cannot create directory")
			}
			err = os.MkdirAll(path.Dir(config.TLSKeyFile), 0744)
			if err != nil {
				log.Fatal().
					Err(err).
					Str("path", path.Dir(config.TLSKeyFile)).
					Msg("I/O error: Cannot create directory")
			}
			errCert2 := ioutil.WriteFile(config.TLSCertFile, []byte{}, 0600)
			errKey2 := ioutil.WriteFile(config.TLSKeyFile, []byte{}, 0600)
			// Check if both files exists now
			if errCert2 == nil && errKey2 == nil {
				// Generate certificates
				log.Warn().Msg("X509 certificate and key are missing")
				GenerateX509CertificateWithAltNames(config, altNames)
			} else {
				// I/O error
				log.Fatal().
					AnErr("errCert", errCert).
					AnErr("errKey", errKey).
					Str("TLSCertFile", config.TLSCertFile).
					Str("TLSKeyFile", config.TLSKeyFile).
					Msg("I/O error: Cannot read files")
			}
		} else {
			// At least the cert or the key exists on the file system
			log.Fatal().
				Err(err).
				Str("TLSCertFile", config.TLSCertFile).
				Str("TLSKeyFile", config.TLSKeyFile).
				Msg("Cannot load X509KeyPair")
		}
	} else {
		// X509 Keypair loaded successfully
		// Check expiry date
		cert, err := x509.ParseCertificate(certs.Certificate[0])
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to parse X509 certificate")
		}
		if time.Now().After(cert.NotAfter) {
			// Cert expired
			log.Error().Msgf("Certificate expired on %v", cert.NotAfter)
			// Generate certificates
			GenerateX509CertificateWithAltNames(config, altNames)
			return
		}
		// Check whether the host matches the existing certificate
		// If not, generate a new one.
		allNames := append([]string{config.Host}, altNames...)

		ips := []net.IP{}
		dnsNames := []string{}

		for _, name := range allNames {
			ip := net.ParseIP(name)
			if ip != nil {
				ips = append(ips, ip)
			} else {
				dnsNames = append(dnsNames, name)
			}
		}

		allCertNames := cert.DNSNames
		for _, ip := range cert.IPAddresses {
			allCertNames = append(allCertNames, ip.String())
		}

		if !equalIP(ips, cert.IPAddresses) || !equalString(dnsNames, cert.DNSNames) {
			log.Warn().Msgf("Host changed from %q to %q generating new certificates", allCertNames, allNames)
			GenerateX509CertificateWithAltNames(config, altNames)
		}
		// Upgrade certificate
		if !cert.IsCA {
			log.Info().Msg("Upgrading TLS certificate...")
			GenerateX509CertificateWithAltNames(config, altNames)
		}
	}
}

// equalIP tells whether a and b contain the same net.IP elements.
// A nil argument is equivalent to an empty slice.
func equalIP(a, b []net.IP) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if !v.Equal(b[i]) {
			return false
		}
	}
	return true
}

// equalString tells whether a and b contain the same strings.
// A nil argument is equivalent to an empty slice.
func equalString(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
